
¿Cual es el stack? 
Cucumber: Herramienta para el desarrollo basado en comportamiento. Permite ejecutar test automatizados escritos en gherkin. 
Behave: Es el equivalente a cucumber para python. python BDD framework. 
Gherkin Lenguaje a bajo nivel para definir features/ el comportamiento del usuario en sistemas. 
Selenium Herramienta para interactuar con la web como lo haría un usuario

Estructura de Gherkin
Given Cumplo una precondición
When Ejecuto una acción
Then Observo este resultado
But No debería poder observar este otro resultado

Estructura de las carpetas
├── features
    ├── login.feature
    └── steps
    |    └── steps.py
    |enviroment.py

login.feature: El test case para probar el login.
steps.py: El código que ejecuta las pruebas del login login.feature.
enviroment.py: Para modificar el entorno en el que estoy probando para poder realizar la prueba en cloud.


Dependencias
Python 3.x
Selenium: pip install selenium
Selenium WebDriver para navegadores web: http://chromedriver.chromium.org/downloads
Behave: pip install behave
Repo behave: https://github.com/behave/behave

Execution
Comando: behave


Documentación
Behave: https://behave.readthedocs.io/en/stable/
Selenium: https://selenium-python.readthedocs.io/


