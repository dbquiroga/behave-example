import time

@given('Go to login form {url}')
def step_impl(context, url):
    context.browser.get(url)

@then('the title should be {text}')
def verify_title(context, text):
    title = context.browser.title
    assert title == text

@when('I enter my credentials')
def enter_credentials(context):
    context.browser.find_element_by_name('ion-input-0').send_keys('guabel1Abel@hotmail.com')
    context.browser.find_element_by_name('ion-input-1').send_keys('Qwerty123')

@when('I click login')
def click_login(context):
    context.browser.find_element_by_xpath('//*[@id="main-content"]/app-login/div[3]/app-auth-form/div/form').submit()

@then('I should be on {url}')
def verify_home(context, url):
    time.sleep(10)
    current_url = context.browser.current_url
    #print(30*"====")
    #print(current_url)
    # import ipdb; ipdb.set_trace() -- breakpoint -- c es para continuar. 
    assert url == current_url

# def see_login_message(context):
#     context.driver.implicitly_wait(10)
#     elem = context.driver.find_element_by_xpath('//*[@id=\"logged-in-message\"]/h2')
#     welcomeText = elem.text
#     try:
#         assert "Welcome tester@crossbrowsertesting.com" == welcomeText
#         set_score(context, 'pass')
#     except AssertionError as e:
#         set_score(context, 'fail')
