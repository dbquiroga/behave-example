import time

@given('Go to register form {url}')
def step_impl(context, url):
    context.browser.get(url)

# necesitaria chequear que el usuario no exista de manera interna

@when('I enter my email')
def enter_registration(context):
    context.browser.find_element_by_xpath('//*[@id="main-content"]/app-register/ion-content/div[1]/app-auth-form/div/form/app-ux-input[1]/div/ion-item/ion-input/input').send_keys('holaaaaaaa@hotmail.com')
    context.browser.find_element_by_xpath('//*[@id="main-content"]/app-register/ion-content/div[1]/app-auth-form/div/form/app-ux-input[2]/div/ion-item/ion-input/input').send_keys('Qwerty123')

@when ('I click in terms and conditions')
def check_terms(context):
    context.browser.find_element_by_xpath('//*[@id="main-content"]/app-register/ion-content/div[1]/app-auth-form/div/form/ion-item/app-ux-checkbox/div/ion-item/ion-checkbox').click()

@when('I click create account')
def submit_register(context):
    #import ipdb; ipdb.set_trace()
    context.browser.find_element_by_xpath('//*[@id="main-content"]/app-register/ion-content/div[1]/app-auth-form/div/form').submit()

@then('Iam on {url}')
def verify_register(context, url):
    time.sleep(10)
    current_url = context.browser.current_url
    #print(30*"====")
    #print(current_url)
    #import ipdb; ipdb.set_trace() # BREAKPOINT -- c es para continuar.
    assert url == current_url

# def see_login_message(context):
#     context.driver.implicitly_wait(10)
#     elem = context.driver.find_element_by_xpath('//*[@id=\"logged-in-message\"]/h2')
#     welcomeText = elem.text
#     try:
#         assert "Welcome tester@crossbrowsertesting.com" == welcomeText
#         set_score(context, 'pass')
#     except AssertionError as e:
#         set_score(context, 'fail')
