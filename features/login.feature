Feature: Login

	Scenario: Logging in
         Given Go to login form https://nonprod.xcapit.com/users/login
         Then the title should be xcapit
         When I enter my credentials
         When I click login
         Then I should be on https://nonprod.xcapit.com/tabs/funds