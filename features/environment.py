from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import requests
import subprocess

#username = "guabel1Abel@hotmail.com"# change this to the username associated with your account
# #/authkey = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjA4NDk4MzMyLCJqdGkiOiI2Y2I0YzcwNzBiYTA0NmYyOTA4YTAyNDU4OGU1MzgwOCIsInVzZXJfaWQiOjkyfQ.7VthNKM9dhSascsaHUFyRJ27loMeTpL93-RkN0ZaFVc"# change this the authkey found on the 'Manage Account' section of our site
def before_feature(context, feature):
#     caps = {}
#     caps['name'] = 'Behave Example'
#     caps['build'] = '1.0'
#     caps['browserName'] = "Chrome"# pulls the latest version of Chrome by default
#     caps['platform'] = "Windows 10"# to specify a version, add caps['version'] = "desired version"
#     caps['screen_resolution'] = '1366x768'
#     caps['record_video'] = 'true'
#     caps['record_network'] = 'false'
#     caps['take_snapshot'] = 'true'
    
    context.browser = webdriver.Chrome("/home/dash/behave-example/chromedriver") #Defino el path para chrome

    # context.driver = webdriver.Remote(
    #    desired_capabilities=caps,
    #    command_executor="https://nonprod.xcapit.com/users/login"
    # )

def after_feature(context, feature):  #Cierre al finalizar cada feature
    context.browser.quit()
#     context.driver.quit()
#     # subprocess.Popen.terminate(context.tunnel_proc)